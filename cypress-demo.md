# Cypress Demo

The sample application uses Nx Workspace, TypeScript, and Cypress with Cucumber for BDD test cases. When the E2E tests are executed with the `--watch` option the test runner will display with the list of tests.

![test runner](./assets//test-runner.png)

Select a test and run.

![test](./assets/test.png)

The workspace contains a script to create a report for the specified tests (i.e., `npm run test:report`).

![report](./assets/report.png)

## Packages

Add the following packages.

```json
    "cucumber-html-reporter": "^5.5.0",
    "cypress-cucumber-preprocessor": "^4.3.1",
```

## Cypress.json

Add a new `cypress.json` file to the root folder of the `e2e` test project in the workspace.

```json
{
  "chromeWebSecurity": false,
  "fileServerFolder": ".",
  "fixturesFolder": "./src/fixtures",
  "ignoreTestFiles": ["**/stepDefinitions/*", "*.js", "*.md"],
  "integrationFolder": "./src/integration",
  "modifyObstructiveCode": false,
  "pluginsFile": "./src/plugins/index",
  "screenshotsFolder": "../../dist/cypress/apps/hello-e2e/screenshots",
  "supportFile": "./src/support/index.ts",
  "testFiles": "**/*.{feature, features}",
  "video": true,
  "videosFolder": "../../dist/cypress/apps/hello-e2e/videos"
}
```

The `cypress.json` file location is shown below in the file tree.

```txt
├── hello-e2e
│   ├── cypress
│   │   └── downloads
│   ├── src
│   │   ├── fixtures
│   │   │   └── example.json
│   │   ├── integration
│   │   │   ├── common
│   │   │   │   └── common.steps.ts
│   │   │   ├── Landing
│   │   │   │   └── landing.steps.ts
│   │   │   └── Landing.feature
│   │   ├── plugins
│   │   │   └── index.js
│   │   └── support
│   │       ├── commands.ts
│   │       └── index.ts
│   ├── .eslintrc.json
│   ├── cypress-cucumber-preprocessor.config.js
│   ├── cypress.json
│   └── tsconfig.json
```

## Cucumber Preprocessor Configuration

Create a `cypress-cucumber-preprocessor.config.js` file for the pre-processor configuration.

> File location: apps/hello-e2e/cypress-cucumber-preprocessor.config.js

Update the file with the following.

```js
const path = require('path');

const stepDefinitionsPath = path.resolve(process.cwd(), './src/integration');
const outputFolder = path.resolve(
  process.cwd(),
  '../../cyreport/cucumber-json'
);

module.exports = {
  nonGlobalStepDefinitions: true,
  stepDefinitions: stepDefinitionsPath,
  cucumberJson: {
    generate: true,
    outputFolder: outputFolder,
    filePrefix: '',
    fileSuffix: '.cucumber',
  },
};
```

## Cypress Plugin

Use to configure the pre-processor options for Cucumber.

> File location: apps/hello-e2e/src/plugins/index.js

```js
const cucumber = require('cypress-cucumber-preprocessor').default;
const browserify = require('@cypress/browserify-preprocessor');

module.exports = (on) => {
  const options = {
    ...browserify.defaultOptions,
    typescript: require.resolve('typescript'),
  };

  on('file:preprocessor', cucumber(options));
};
```

## Reports

Create a report configuration file in the `tools` folder of the Nx workspace.

> File location: tools/generate-cucumber-report.js

Add the following to the file.

```js
const path = require('path');
const reporter = require('cucumber-html-reporter');
const chalk = require('chalk');

const options = {
  theme: 'bootstrap',
  jsonDir: path.join(process.cwd(), 'cyreport/cucumber-json'),
  output: path.join(process.cwd(), 'cyreport/cucumber_report.html'),
  reportSuiteAsScenarios: true,
  scenarioTimestamp: true,
  launchReport: false,
  storeScreenshots: true,
  noInlineScreenshots: false,
};

try {
  reporter.generate(options);
} catch (e) {
  console.log(chalk.red(`Could not generate cypress reports`));
  console.log(chalk.red(`${e}`));
}
```

## Scripts

Run the `test` script to execute the feature test.

- test: Use the test script to run the `e2e` tests: `npm run test`

The `--watch` option is required to view/use the test runner.

```json
"scripts": {
    ...
    "test": "nx e2e hello-e2e --watch --verbose > output.txt",
    "test:report ": "node ./tools/generate-cucumber-report.js"
  }
```

The `test` script will create a JSON file for the specified test/feature.

```json
[
  {
    "keyword": "Feature",
    "name": "Landing page",
    "line": 1,
    "id": "landing-page",
    "tags": [],
    "uri": "Landing.feature",
    "elements": [
      {
        "id": "landing-page;the-application-displays-the-welcome-message.",
        "keyword": "Scenario",
        "line": 3,
        "name": "The application displays the welcome message.",
        "tags": [],
        "type": "scenario",
        "steps": [
          {
            "arguments": [],
            "keyword": "Given ",
            "line": 4,
            "name": "the user is logged in",
            "result": {
              "status": "passed",
              "duration": 538000000
            }
          },
          {
            "arguments": [],
            "keyword": "Then ",
            "line": 5,
            "name": "the \"Welcome to hello!\" message is displayed",
            "result": {
              "status": "passed",
              "duration": 28000000
            }
          }
        ]
      }
    ]
  }
]
```

Run the `test:report` script to create the Cucumber Report.

- test:report: Use to create reports in the

The terminal output indicates the report script executed and created the HTML file for the specified report.

```ts
npm run test:report
> cypress-demo@0.0.0 test:report /Users/vvaughn/work/proofs/cypress-demo
> node ./tools/generate-cucumber-report.js

🚀 Cucumber HTML report /Users/vvaughn/work/proofs/cypress-demo/cyreport/cucumber_report.html generated successfully 👍
```

The JSON is generated for each of the report items and displayed using the HTML file. 

> 

```json
[
  {
    "keyword": "Feature",
    "name": "Landing page",
    "line": 1,
    "id": "landing-page",
    "tags": [],
    "uri": "Landing.feature",
    "elements": [
      {
        "id": "landing-page;the-application-displays-the-welcome-message.",
        "keyword": "Scenario",
        "line": 3,
        "name": "The application displays the welcome message.",
        "tags": [],
        "type": "scenario",
        "steps": [
          {
            "arguments": [],
            "keyword": "Given ",
            "line": 4,
            "name": "the user is logged in",
            "result": {
              "status": "passed",
              "duration": 538000000
            }
          },
          {
            "arguments": [],
            "keyword": "Then ",
            "line": 5,
            "name": "the \"Welcome to hello!\" message is displayed",
            "result": {
              "status": "passed",
              "duration": 28000000
            }
          }
        ]
      }
    ]
  }
]
```
